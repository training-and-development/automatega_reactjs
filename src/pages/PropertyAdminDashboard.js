import React from 'react'
import Container from "@material-ui/core/Container";
import Header from "../component-features/header/Header.component";
import HeaderTabs from "../component-features/headerTabs/HeaderTabs.component";

function PropertyAdminDashboard() {
    return (
        <Container maxWidth="95%">
            <Header />
            <HeaderTabs showTabIndex={2} />
            <div>PropertyAdminDashboard</div>
        </Container>

    )
}

export default PropertyAdminDashboard