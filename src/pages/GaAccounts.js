import React from 'react'
import Container from "@material-ui/core/Container";
import Header from "../component-features/header/Header.component";
import HeaderTabs from "../component-features/headerTabs/HeaderTabs.component";

function GaAccounts() {
    return (
        <Container maxWidth="95%">
            <Header />
            <HeaderTabs showTabIndex={4} />
            <div>GaAccounts</div>
        </Container>

    )
}

export default GaAccounts