import React from "react";
import Container from "@material-ui/core/Container";
import Header from "../component-features/header/Header.component";
import HeaderTabs from "../component-features/headerTabs/HeaderTabs.component";
import UserDetails from "../component-features/userdetails/UserDetailsComponent";

function UserDetailspage() {
  return (
    <Container maxWidth="95%">
      <Header />
      <HeaderTabs />
      <UserDetails />
    </Container>
  );
}

export default UserDetailspage;
