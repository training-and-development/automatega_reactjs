import React from "react";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import useStyles from "../component-features/home/HomeComponent.style";
import { Box } from "@material-ui/core";
import Header from "../component-features/header/Header.component";
import HeaderTabs from "../component-features/headerTabs/HeaderTabs.component";
import Button from "../common-components/buttons/Button"
import DataImportJob from "../component-features/dataImport/DataImportJob.component";
import DataImportApex from "../component-features/dataImport/DataImportApex.component";
import { Cpu } from "react-feather";
import DataImportJobTables from "../component-features/dataImport/DataImportJobTables.component";
import { FAILED_JOB_LIST_URL, JOBS_URL } from "../shared/constants";
import useFetch from "../shared/useFetch";
// Another Example for custom hook
// import useFetchAxios from "../shared/useFetchAxios";

const DataImportDashboard = () => {
  const classes = useStyles();
  const failed_url = FAILED_JOB_LIST_URL;
  const job_url = JOBS_URL;
  const { data: failedData, loading, error } = useFetch(failed_url);
  const { data: jobData } = useFetch(job_url);
  // Another Example for custom hook
  // const { data: jobData } = useFetchAxios(job_url);
  if (error) console.log("Server Connection Error", error)

  return (
    <Container maxWidth="95%">
      <Header />
      <HeaderTabs showTabIndex={1} />
      <Box sx={{ display: "flex", justifyContent: "space-between", marginTop: "20px" }}>
        <Typography variant="h5" className={classes.data}>
          Data Import Dashboard
        </Typography>
        <div>
          <Button variant='primary' icon={<Cpu color="#ffffff" style={{ "paddingRight": "5px" }} />} text="Google Analytics - Data Import" data-testid="lgn-btn" />
        </div>
      </Box>
      <DataImportApex />
      <br />
      <br />
      <DataImportJob />
      <br />
      <br />
      {loading && <p>Loading...</p>}
      <DataImportJobTables failedRows={failedData} jobRows={jobData} />
      <br />
    </Container>
  );
};

export default DataImportDashboard;
