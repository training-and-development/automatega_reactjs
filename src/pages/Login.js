import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import LoginForm from "../component-features/login/LoginForm";
import AuthApi from "../api/AuthApi";
import Cookies from "js-cookie";

function Login() {
  const adminUser = {
    admin: "admin@softcrylic.com",
    password: "Kalkaji@123",
  };
  const [user, setUser] = useState({ email: "" });
  const [error, setError] = useState("");
  const Auth = useContext(AuthApi);
  const navigate = useHistory();
  const Login = (details) => {
    if (
      details.email === adminUser.admin &&
      details.password === adminUser.password
    ) {
      setUser({
        email: details.email,
      });
      Auth.setAuth(true);
      Cookies.set("user", "loginTrue");;
    } else {
      setError("Invalid credentials, please try again!");
    }
  };
  return (
    <>
      {user.email !== "" ? (
        navigate.push("/home")
      ) : (
        <LoginForm Login={Login} error={error} />
      )}
    </>
  );
}

export default Login;
