import React from "react";
import Container from "@material-ui/core/Container";
import Header from "../component-features/header/Header.component";
import HeaderTabs from "../component-features/headerTabs/HeaderTabs.component";

function Homepage() {
  return (
    <Container maxWidth="95%" data-testid="homepage" name="homepage">
      <Header />
      <HeaderTabs showTabIndex={0} />
    </Container>
  );
}

export default Homepage;
