import React from 'react'
import Container from "@material-ui/core/Container";
import Header from "../component-features/header/Header.component";
import HeaderTabs from "../component-features/headerTabs/HeaderTabs.component";

function AuditDashboard() {
    return (
        <Container maxWidth="95%">
            <Header />
            <HeaderTabs showTabIndex={3} />
            <div>AuditDashboard</div>
        </Container>

    )
}

export default AuditDashboard