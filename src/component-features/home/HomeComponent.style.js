import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  user: {
    fontFamily: "Quicksand",
    fontWeight: 600,
    paddingTop: "15px",
    paddingBottom: "40px",
    fontSize: "28px",
    marginTop: "15px",
    color: "#3b3f5c",
    letterSpacing: "0.0312rem",
  },
  info: {
    fontFamily: "Quicksand",
    paddingTop: "15px",
    fontWeight: 550,
    paddingBottom: "40px",
    fontSize: "20px",
    color: "#3b3f5c",
  },
  data: {
    fontFamily: "Quicksand",
    fontWeight: 600,
    paddingBottom: "20px",
    fontSize: "26px",
    color: "#3b3f5c",
  },
}));
