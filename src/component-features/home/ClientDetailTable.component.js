import * as React from "react";
import ClientUserTable from "../../common-components/table/ClientTable";

const columns = [
  { id: "clientName", label: "NAME", minWidth: 170 },
  { id: "bucketName", label: "AWS S3 BUCKET NAME", minWidth: 100 },
  {
    id: "dataImportAccess",
    label: "DATA IMPORT ACCESS",
    minWidth: 170,
  },
  {
    id: "propertyAdminAccess",
    label: "PROPERTY ADMIN ACCESS",
    minWidth: 170,
  },
  {
    id: "auditAccess",
    label: "AUDIT ACCESS",
    minWidth: 170,
  },
  {
    id: "edit",
    label: "EDIT",
    minWidth: 170,
    format: (value) => value.toLocaleString("en-US"),
  },
];

const ClientDetailTable = (props) => {
  const { data } = props;
  return (
    <ClientUserTable rows={data} columns={columns} />
  );
};

export default ClientDetailTable;
