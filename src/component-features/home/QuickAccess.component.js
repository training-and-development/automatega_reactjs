import * as React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { Cpu, File, Box as BoxIcon, Feather, Users } from "react-feather";
import QuickAccessCards from "../../common-components/quick-access-cards/QuickAccessCards";

const QuickAccess = (props) => {
  const { data } = props;
  const icons = [<Cpu color="#e21b76" size={50} strokeWidth={1} />, <Feather color="#c54922" size={50} strokeWidth={1} />, <BoxIcon color="#3D8911" size={50} strokeWidth={1} />, <File color="#8D5C51" size={50} strokeWidth={1} />, <Users color="#8256c5" size={50} strokeWidth={1} />];
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {Array.from(Array(data.length)).map((_, index) => (
          <QuickAccessCards icon={icons[index]} title={data[index].title} desc={data[index].desc} index={index} />
        ))}
      </Grid >
    </Box >
  );
};

export default QuickAccess;
