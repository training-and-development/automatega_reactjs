import React from 'react';
import Typography from "@material-ui/core/Typography";
import useStyles from "./HomeComponent.style";
import QuickAccess from "./QuickAccess.component";
import ClientDetailTable from "./ClientDetailTable.component";
import { CLIENTS_URL, QUICK_ACCESS_DATA } from "../../shared/constants";
import useFetch from '../../shared/useFetch';
// import { clientDetailRows } from "../../shared/utils";

function HomeComponent() {
  const classes = useStyles();
  const username = "Bhupendra";
  const url = CLIENTS_URL;
  const { data, loading, error } = useFetch(url);
  if (error) console.log("Server Connection Error", error)

  return (
    <>
      <Typography variant="h3" className={classes.user}>
        Hi {username} !
      </Typography>
      <Typography variant="h5" className={classes.info}>
        Quick Access
      </Typography>
      <QuickAccess data={QUICK_ACCESS_DATA} />
      <br />
      {loading && <p>Loading...</p>}
      <ClientDetailTable data={data} />
    </>
  );
}

export default HomeComponent;
