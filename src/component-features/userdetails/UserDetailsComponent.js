import React from 'react';
import Typography from "@material-ui/core/Typography";
// import { userDetailRows } from "../../shared/utils";
import { Box } from "@material-ui/core";
import UserDetailsTable from "./UserDetailsTable.component";
import useStyles from "./UserDetailsComponent.style";
import Button from "../../common-components/buttons/Button";
import { Plus } from "react-feather";
import { ALL_USERS_URL } from '../../shared/constants';
import useFetch from '../../shared/useFetch';

function UserDetails() {
  const classes = useStyles();
  const url = ALL_USERS_URL;
  const { data, loading, error } = useFetch(url);
  if (error) console.log("Server Connection Error", error)

  return (
    <>
      <br />
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5" className={classes.info}>
          Users Detail
        </Typography>
        <div>
          <Button variant='primary' icon={<Plus className={classes.addUserBtn} />} text="Add New User" />
        </div>
      </Box>
      {loading && <p>Loading...</p>}
      <UserDetailsTable data={data} />
    </>
  );
}

export default UserDetails;
