import * as React from "react";
import ClientUserTable from "../../common-components/table/ClientTable";

const column = [
  { id: "email", label: "EMAIL", minWidth: 170 },
  { id: "firstName", label: "FIRST NAME", minWidth: 170 },
  {
    id: "lastName",
    label: "LAST NAME",
    minWidth: 170,
  },
  {
    id: "clientId",
    label: "CLIENT",
    minWidth: 170,
  },
  {
    id: "active",
    label: "ACTIVE",
    minWidth: 70,
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "isEditable",
    label: "",
    minWidth: 100,
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "isDeleteable",
    label: "",
    minWidth: 100,
    format: (value) => value.toLocaleString("en-US"),
  },
];

const UserDetailsTable = (props) => {
  const { data } = props;
  return (
    <ClientUserTable rows={data} columns={column} />
  );

};

export default UserDetailsTable;
