import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  info: {
    fontFamily: "Quicksand",
    fontWeight: "bold",
    paddingBottom: "40px",
  },
  addUserBtn: {
    paddingRight: "5px",
  },
}));
