import { makeStyles } from "@material-ui/core/styles";
import { padding } from "@mui/system";

export default makeStyles((theme) => ({
  root: {
    backgroundColor: "#191e3a",
    borderRadius: "10px",
    minHeight: "55px !important",
  },
  withDivider: {
    borderRight: `1px solid #ffffff !important`,
    padding: "0 30px 0 0"
  },
  title: {
    fontFamily: "Quicksand !important",
    fontWeight: "400 !important",
    letterSpacing: "1px !important",
    fontSize: "13px !important",
    color: "#fafafa !important",
    textTransform: "capitalize !important"
  },
  icon: {
    verticalAlign: "bottom",
    margin: "0 8px 0 0",
    color: "#e0e6ed",
    strokeWidth: "1.5px"
  }
}));
