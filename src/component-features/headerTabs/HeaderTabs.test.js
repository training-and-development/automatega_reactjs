import { render, screen } from "@testing-library/react";
import HeaderTabs from "./HeaderTabs.component";

const renderHeaderTabs = () => {
  return render(<HeaderTabs />);
};

describe("HeaderTabs Component", () => {
  it("should have the navigational tabs", () => {
    renderHeaderTabs();
    expect(screen.getByTestId("report-nav-tabs")).toBeInTheDocument();
  });

  it("HeaderTabs should render all the tabs and should be present in document", () => {
    renderHeaderTabs();
    [
      "Home",
      "Data Import Dashboard",
      "Property Admin Dashboard",
      "Audit Dashboard",
      "GA Accounts",
    ].forEach((text, index) => {
      expect(screen.getByTestId(`report-tab-${index + 1}`)).toHaveTextContent(
        text
      );
    });
  });

  it("on clicking tab 1 it should show only the respective tab panel and others should be hidden", () => {
    renderHeaderTabs();
    screen.getByTestId(`report-tab-1`).click();
    expect(screen.getByTestId("report-tabpanel-1")).toBeVisible();
    expect(screen.getByTestId("report-tabpanel-2")).not.toBeVisible();
    expect(screen.getByTestId("report-tabpanel-3")).not.toBeVisible();
    expect(screen.getByTestId("report-tabpanel-4")).not.toBeVisible();
    expect(screen.getByTestId("report-tabpanel-5")).not.toBeVisible();
  });

  it("on clicking tab 3 it should show only the respective tab panel and others should be hidden", () => {
    renderHeaderTabs();
    screen.getByTestId(`report-tab-3`).click();
    expect(screen.getByTestId("report-tabpanel-1")).not.toBeVisible();
    expect(screen.getByTestId("report-tabpanel-2")).not.toBeVisible();
    expect(screen.getByTestId("report-tabpanel-3")).toBeVisible();
    expect(screen.getByTestId("report-tabpanel-4")).not.toBeVisible();
    expect(screen.getByTestId("report-tabpanel-5")).not.toBeVisible();
  });
});
