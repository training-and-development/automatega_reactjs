import * as React from "react";
import { useHistory } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import useStyles from "./HeaderTabs.styles";
import HomeComponent from "../home/HomeComponent";
import DataImportDashboard from "../../pages/DataImportDashboard";
// import PropertyAdminDashboard from "../../pages/PropertyAdminDashboard";
// import AuditDashboard from "../../pages/AuditDashboard";
// import GaAccounts from "../../pages/GaAccounts";
import { Cpu, Home, File, Box as BoxIcon } from "react-feather";

const HeaderTabs = (props) => {
  const classes = useStyles();
  const navigate = useHistory();
  const { showTabIndex } = props;
  const [value, setValue] = React.useState(
    showTabIndex !== undefined ? showTabIndex : false
  );
  const handleChange = (_, newValue) => setValue(newValue);

  return (
    <>
      <AppBar position="static" className={classes.root}>
        <Toolbar className={classes.root}>
          <Tabs
            data-testid="report-nav-tabs"
            value={value}
            onChange={handleChange}
            TabIndicatorProps={{ style: { display: "none" } }} S
          >
            <Tab
              label={
                <Grid display="flex" alignItems="center" className={classes.withDivider}>
                  <Home className={classes.icon} />
                  <Typography className={classes.title}>Home</Typography>
                </Grid>
              }
              data-testid={`report-tab-1`}
              onClick={() => navigate.push('/home')}
            />
            <Tab
              label={
                <Grid display="flex" alignItems="center" className={classes.withDivider}>
                  <Cpu className={classes.icon} />
                  <Typography className={classes.title}>Data Import Dashboard</Typography>
                </Grid>
              }
              data-testid={`report-tab-2`}
              onClick={() => navigate.push('/dataimportdashboard')}
            />
            <Tab
              label={
                <Grid display="flex" alignItems="center" className={classes.withDivider}>
                  <BoxIcon className={classes.icon} />
                  <Typography className={classes.title}>Property Admin Dashboard</Typography>
                </Grid>
              }
              data-testid={`report-tab-3`}
              onClick={() => navigate.push('/propertyadmin')}
            />
            <Tab
              label={
                <Grid display="flex" alignItems="center" className={classes.withDivider}>
                  <File className={classes.icon} />
                  <Typography className={classes.title}>Audit Dashboard</Typography>
                </Grid>
              }
              data-testid={`report-tab-4`}
              onClick={() => navigate.push('/auditdashboard')}
            />
            <Tab
              label={
                <Grid display="flex" alignItems="center">
                  <File className={classes.icon} />
                  <Typography className={classes.title}>GA Accounts</Typography>
                </Grid>
              }
              data-testid={`report-tab-5`}
              onClick={() => navigate.push('/gaaccountsd')}
            />
          </Tabs>
        </Toolbar>
      </AppBar>
      <div index={value} onChangeIndex={setValue}>
        <TabPanel value={value} index={0} data-testid="report-tabpanel-1">
          <HomeComponent />
        </TabPanel>
        <TabPanel value={value} data-testid="report-tabpanel-2">
          <DataImportDashboard />
        </TabPanel>
        <TabPanel value={value} index={2} data-testid="report-tabpanel-3">
          {/* <PropertyAdminDashboard /> */}
        </TabPanel>
        <TabPanel value={value} index={3} data-testid="report-tabpanel-4">
          {/* <AuditDashboard /> */}
        </TabPanel>
        <TabPanel value={value} index={4} data-testid="report-tabpanel-5">
          {/* <GaAccounts /> */}
        </TabPanel>
      </div>
    </>
  );
};

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div role="tabpanel" hidden={value !== index} {...other}>
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default HeaderTabs;
