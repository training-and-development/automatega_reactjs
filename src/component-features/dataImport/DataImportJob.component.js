import React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import DataImportCard from "../../common-components/dataimportcard/DataImportCard"
import { Send, ThumbsUp, ThumbsDown } from "react-feather";
import { DATA_IMPORT_JOB_STATUS_DATA } from "./utils";

export const data = DATA_IMPORT_JOB_STATUS_DATA;

const DataImportJob = () => {
  const icons = [
    <Send color="#ffffff" size={50} strokeWidth={1} />,
    <ThumbsUp color="#ffffff" size={50} strokeWidth={1} />,
    <ThumbsDown color="#ffffff" size={50} strokeWidth={1} />,
  ];

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
      >
        {Array.from(Array(data.length)).map((_, index) => (
          <DataImportCard icon={icons[index]} title={data[index].title} value={data[index].value} index={index} />
        ))}
      </Grid >
    </Box >
  );
};

export default DataImportJob;
