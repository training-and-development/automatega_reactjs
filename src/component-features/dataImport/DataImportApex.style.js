import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  chartBg: {
    backgroundColor: "#ffffff !important",
  },
  data: {
    fontFamily: "Quicksand",
    fontWeight: 700,
    paddingBottom: "20px",
    fontSize: "19px",
    color: "#515365",
  },
}));
