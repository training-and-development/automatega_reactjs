import * as React from "react";
import DataImportSortableTable from "../../common-components/data-import-table/DataImportSortableTable";

const failedJobColumn = [
  { id: "fromEmail", label: "REQUEST FROM", minWidth: 150 },
  { id: "filename", label: "FILE NAME", minWidth: 150 },
  {
    id: "extractionSetupId",
    label: "CUSTOMER DATA SOURCE ID",
    minWidth: 170,
  },
  // {
  //   id: "webPropertyId",
  //   label: "ID",
  //   minWidth: 170,
  // },
  // {
  //   id: "webPropertyName",
  //   label: "Name",
  //   minWidth: 170,
  // },
  {
    id: "excpMsg",
    label: "MESSAGE",
    minWidth: 50,
  },
  {
    id: "jobStatus",
    label: "JOB STATUS",
    minWidth: 10,
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "modificationDate",
    label: "FILE RECEIVED",
    minWidth: 170,
    format: (value) => value.toLocaleString("en-US"),
  }
];

const jobColumns = [
  { id: "fromEmail", label: "REQUEST FROM", minWidth: 150 },
  { id: "filename", label: "FILE NAME", minWidth: 150 },
  {
    id: "extractionSetupId",
    label: "CUSTOMER DATA SOURCE ID",
    minWidth: 170,
  },
  {
    id: "jobStatus",
    label: "JOB STATUS",
    minWidth: 10,
    format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "creationDate",
    label: "FILE RECEIVED",
    minWidth: 170,
    format: (value) => value.toLocaleString("en-US"),
  }
];

const DataImportJobTables = (props) => {

  const { failedRows, jobRows } = props;
  return (
    <>
      <DataImportSortableTable rows={failedRows} columns={failedJobColumn} searchBy='requestFrom' tableTitle='Data Import Job Failed due to Incorrect Permission issue' />
      <br />
      <br />
      <DataImportSortableTable rows={jobRows} columns={jobColumns} searchBy='requestFrom' tableTitle='Data Import Job Details' />
    </>
  );

};

export default DataImportJobTables;
