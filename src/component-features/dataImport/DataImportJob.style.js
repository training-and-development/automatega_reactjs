import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  jobTitle: {
    fontFamily: "Quicksand !important",
    fontWeight: "bold !important",
    letterSpacing: "2px !important",
    fontSize: "20px !important",
  },
  jobCount: {
    fontFamily: "Quicksand !important",
  },
  firstStyle: {
    backgroundColor: "#1c55e261 !important",
  },
  secondStyle: {
    backgroundColor: "rgb(141 191 66 / 44%) !important",
  },
  thirdStyle: {
    backgroundColor: "#f3676b4f !important",
  },
  sendCount: {
    color: "rgb(27, 85, 226) !important",
  },
  upCount: {
    color: "rgb(141, 191, 66) !important",
  },
  downCount: {
    color: "#f3676b !important",
  },
  sendIcon: {
    backgroundColor: "rgb(27, 85, 226) !important",
    display: "inline-block",
    marginTop: "-28px",
    padding: "8px",
    borderRadius: "6px",
  },
  upIcon: {
    backgroundColor: "rgb(141, 191, 66) !important",
    display: "inline-block",
    marginTop: "-28px",
    padding: "8px",
    borderRadius: "6px",
  },
  downIcon: {
    backgroundColor: "#f3676b !important",
    display: "inline-block",
    marginTop: "-28px",
    padding: "8px",
    borderRadius: "6px",
  },
}));
