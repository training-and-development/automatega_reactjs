export const DATA_IMPORT_JOB_STATUS_DATA = [
  {
    title: "Total Data Import Job Ran",
    value: 562,
  },
  {
    title: "Successful Data Import",
    value: 431,
  },
  {
    title: "Failed Data Import",
    value: 130,
  },
];

//Data Import failed Jobs
function dataImportFailedData(
  requestFrom,
  fileName,
  customerDataSourceId,
  message,
  jobStatus,
  fileReceived,
) {
  return {
    requestFrom,
    fileName,
    customerDataSourceId,
    message,
    jobStatus,
    fileReceived,
  };
}

export const dataImportFailedRows = [
  dataImportFailedData("microsoft@powerapps.com", "MC_ID_2021-01-09T14:00:03.6684089Z.csv", { data: 'mVoMeo9QRkOHr4T', id: 'UA-96613125-1', name: '010410-WEB CON BRND-FANTA NAG en Fanta NAG - 2016 AEM brand.com codebase' }, 'Insufficient Permissions', false, '2021-01-09 09:02:14.0'),
  dataImportFailedData("microsoft@powerapps.com", "WC_ID_2021-01-09T14:00:03.6684089Z.csv", { data: '42-mVoMeo9QRkOHr4T', id: 'UA-56613125-1', name: '010421-WEB CON BRND-FANTA NAG en Fanta NAG - 2016 AEM brand.com codebase' }, 'Insufficient Permissions', false, '2021-01-09 09:02:12.0'),
];

//Data Import Jobs
function dataImportJobData(
  requestFrom,
  fileName,
  customerDataSourceId,
  jobStatus,
  fileReceived,
) {
  return {
    requestFrom,
    fileName,
    customerDataSourceId,
    jobStatus,
    fileReceived,
  };
}

export const dataImportJobRows = [
  dataImportJobData("microsoft@powerapps.com", "MC_ID_2021-01-09T14:00:03.6684089Z.csv", { data: 'mVoMeo9QRkOHr4T', id: 'UA-96613125-1', name: '010410-WEB CON BRND-FANTA NAG en Fanta NAG - 2016 AEM brand.com codebase' }, true, '2021-01-09 09:02:14.0'),
  dataImportJobData("microsoft@powerapps.com", "WC_ID_2021-01-09T14:00:03.6684089Z.csv", { data: '42-mVoMeo9QRkOHr4T', id: 'UA-56613125-1', name: '010421-WEB CON BRND-FANTA NAG en Fanta NAG - 2016 AEM brand.com codebase' }, true, '2021-01-09 09:02:12.0'),
];
