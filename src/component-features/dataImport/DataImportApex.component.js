import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { experimentalStyled as styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import useStyles from "./DataImportApex.style";
import ReactApexChart from "react-apexcharts";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "left",
  color: theme.palette.text.secondary,
  height: "100%",
}));

const DataImportApex = () => {
  const classes = useStyles();

  const [chartData, setChartData] = useState({
    series: [
      {
        name: "series1",
        data: [31, 40, 28, 51, 42, 109, 100],
      },
      {
        name: "series2",
        data: [11, 32, 45, 32, 34, 52, 41],
      },
    ],
    options: {
      legend: {
        show: false,
      },
      fill: {
        colors: ["rgb(231, 81, 90)", "rgb(40, 167, 69)", "rgb(226, 185, 33)"],
      },
      colors: ["rgb(231, 81, 90)", "rgb(40, 167, 69)", "rgb(226, 185, 33)"],
      chart: {
        height: 350,
        type: "area",
        fontFamily: 'Quicksand',
        toolbar: {
          show: false,
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      xaxis: {
        categories: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec",
        ],
      },
      tooltip: {
        x: {
          format: "MMM",
        },
      },
    },
  });

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={{ xs: 12 }} columns={{ xs: 12 }}>
        <Grid item xs={12} key={1}>
          <Item className={classes.chartBg}>
            <Typography variant="h5" className={classes.data}>
              Data Import Job Status
            </Typography>
            <ReactApexChart
              options={chartData.options}
              series={chartData.series}
              type="area"
              height={350}
            />
          </Item>
        </Grid>
      </Grid>
    </Box>
  );
};

export default DataImportApex;
