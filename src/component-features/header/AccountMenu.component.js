import React, { useState, useEffect, useContext } from 'react';
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import MenuComp from "../../common-components/menu/MenuComp";
import MenuItemComp from "../../common-components/menu-item/MenuItemComp";
import { Feather, User, Users, LogOut, ChevronDown } from "react-feather";
import { useHistory } from "react-router-dom";
import IconButtonComp from '../../common-components/icon-button/IconButtonComp';
import Tooltip from "@mui/material/Tooltip";
import useStyles from "./AccountMenu.styles";
import AuthApi from "../../api/AuthApi";
import Cookies from "js-cookie";

export default function AccountMenu() {
  const navigate = useHistory();
  const [anchorEl, setAnchorEl] = useState(null);
  const [clientAnchorEl, setclientAnchorEl] = useState(null);
  const [page, setPage] = useState(null);
  const open = Boolean(anchorEl);
  const [text, setText] = useState("Randstad USA");
  const openCientMenu = Boolean(clientAnchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClientClick = (event) => {
    setclientAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleClientClose = () => {
    setclientAnchorEl(null);
  };

  useEffect(() => {
    if (page != null) {
      //onboard or user or signout
      navigate.push(page);
    }
  }, [page]);

  // for handling Logout
  const Auth = useContext(AuthApi);
  const clickHandler = () => {
    Auth.setAuth(false);
    Cookies.remove("user");
  };

  return (
    <Grid style={{ float: "right" }} >
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Grid>
          <IconButtonComp
            onClick={handleClientClick}
            aria-controls={openCientMenu ? "client-menu" : undefined}
            varient={'secondary-title'}
            text={text}
          />
        </Grid>
        <MenuComp
          anchorEl={clientAnchorEl}
          id="client-menu"
          open={openCientMenu}
          onClose={handleClientClose}
          onClick={handleClientClose}
          varient={'menus'}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 3,
              padding: "10px",
              "& .MuiAvatar-root": {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: "background.paper",
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
          menuItem1={<MenuItemComp divider={false} onClick={() => setText("Randstad USA")} style={{ padding: "8px 15px", width: "150px", height: "24px" }} iconText="Randstad USA" />}
          menuItem2={<MenuItemComp divider={false} onClick={() => setText("Coke")} style={{ padding: "8px 15px", width: "150px", height: "24px" }} iconText="Coke" />}
        />
        <Grid>
          <Tooltip title="Account settings">
            <IconButtonComp
              onClick={handleClick}
              aria-controls={open ? "account-menu" : undefined}
              varient={'primary-title'}
              icon1={<User data-testid="user-icon" size={15} color="#888EA8" />}
              icon2={<ChevronDown data-testid="down-icon" size={15} color="#888EA8" />}
              text="Bhupendra"
            />
          </Tooltip>
        </Grid>

        <MenuComp
          anchorEl={anchorEl}
          id="account-menu"
          open={open}
          onClose={handleClose}
          onClick={handleClose}
          varient={'menus'}
          PaperProps={{
            elevation: 0,
            sx: {
              overflow: "visible",
              filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
              mt: 4,
              padding: 1,
              "& .MuiAvatar-root": {
                width: 32,
                height: 32,
                ml: -0.5,
                mr: 1,
              },
              "&:before": {
                content: '""',
                display: "block",
                position: "absolute",
                top: 0,
                right: 14,
                width: 10,
                height: 10,
                bgcolor: "background.paper",
                transform: "translateY(-50%) rotate(45deg)",
                zIndex: 0,
              },
            },
          }}
          transformOrigin={{ horizontal: "right", vertical: "top" }}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
          menuItem1={<MenuItemComp divider={true} onClick={() => setPage("onboard")} icon={<Feather data-testid="feather-icon" size={18} />} iconText="On Board Client" />}
          menuItem2={<MenuItemComp divider={true} onClick={() => setPage("users")} icon={<Users data-testid="users-icon" size={18} />} iconText="Users" />}
          menuItem3={<MenuItemComp divider={true} onClick={clickHandler} icon={<LogOut data-testid="logout-icon" size={18} />} iconText="Sign Out" />}
        />
      </Box>
    </Grid>
  );
}
