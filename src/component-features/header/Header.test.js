import { render, screen } from "@testing-library/react";
import Header from "./Header.component";

const renderHeader = () => {
  return render(<Header />);
};

describe("Header Component", () => {
  it("should have a logo title", () => {
    renderHeader();
    const logoText = screen.queryByText(/AutomateGA/i);
    expect(logoText).toBeInTheDocument();
  });
});
