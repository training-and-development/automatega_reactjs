import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  menus: {
    '& li > svg': {
      marginRight: '7px',
      fill: 'rgba(25, 30, 58, 0.19)',
    }
  },
  title: {
    fontFamily: "Quicksand !important",
    fontWeight: "bold !important",
    letterSpacing: "1px !important",
    margin: "0 5px 0 0",
    fontSize: "13px !important"
  }
}));
