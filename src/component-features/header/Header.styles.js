import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  logoTitle: {
    fontFamily: "Quicksand !important",
    fontWeight: "bold !important",
    letterSpacing: "1px !important",
    fontSize: "20px !important",
    color: "#060818 !important"
  },
  logo: {
    padding: "10px 0 20px 0"
  }
}));
