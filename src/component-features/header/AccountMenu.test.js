import { render, screen } from "@testing-library/react";
import AccountMenu from "./AccountMenu.component";

const renderAccountMenu = () => {
  return render(<AccountMenu />);
};

describe("AccountMenu Component", () => {
  it("should have a location", () => {
    renderAccountMenu();
    const location = screen.getByTestId("location");
    expect(location).toBeInTheDocument();
  });

  it("should have a username", () => {
    renderAccountMenu();
    const username = screen.getByTestId("username");
    expect(username).toBeInTheDocument();
  });

  it("should have a account menu button", () => {
    renderAccountMenu();
    const accMenu = screen.getByTestId("account-menu-btn");
    expect(accMenu).toBeInTheDocument();
  });

  it("should have only user icon before clicking menu button", () => {
    renderAccountMenu();

    const userIcon = screen.getByTestId("user-icon");
    expect(userIcon).toBeInTheDocument();

    const featherIcon = screen.queryByTestId("feather-icon");
    expect(featherIcon).not.toBeInTheDocument();

    const usersIcon = screen.queryByTestId("users-icon");
    expect(usersIcon).not.toBeInTheDocument();

    const logoutIcon = screen.queryByTestId("logout-icon");
    expect(logoutIcon).not.toBeInTheDocument();
  });

  it("should have menu list feather icons after clicking menu button", () => {
    renderAccountMenu();

    screen.getByTestId(`account-menu-btn`).click();
    const featherIcon = screen.getByTestId("feather-icon");
    expect(featherIcon).toBeInTheDocument();

    const usersIcon = screen.getByTestId("users-icon");
    expect(usersIcon).toBeInTheDocument();

    const logoutIcon = screen.getByTestId("logout-icon");
    expect(logoutIcon).toBeInTheDocument();
  });
});
