import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import useStyles from "./Header.styles";
import logo from "../../assets/images/90x90.jpg";
import AccountMenu from "./AccountMenu.component";

export default function Header() {
  const classes = useStyles();

  return (
    <>
      <CssBaseline />
      <Grid container className={classes.logo}>
        <Grid width="30%" display="flex" alignItems="center">
          <Box
            component="img"
            sx={{
              height: "40px",
              width: "40px",
              borderRadius: "5px",
            }}
            alt="logo"
            src={logo}
          />
          &nbsp;
          <Typography variant="h5" className={classes.logoTitle}>
            AutomateGA
          </Typography>
        </Grid>
        <Grid width="70%">
          <AccountMenu />
        </Grid>
      </Grid>
    </>
  );
}
