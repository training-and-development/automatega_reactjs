import React, { useState } from "react";
import {
  Box,
  Grid,
  Typography,
} from "@material-ui/core";
import { Lock } from "react-feather";
import Button from "../../common-components/buttons/Button";
import Textbox from "../../common-components/textbox/textbox";
import DSwitch from "../../common-components/switch/Switch";
import useStyles from "./loginStyles";
import channel from "../../assets/images/omnichannel-personalization.png";

function LoginForm({ Login, error }) {
  const classes = useStyles();
  const [details, setDetails] = useState({ email: "", password: "" });

  const [passwordShown, setPasswordShown] = useState(false);
  const submitHandler = (e) => {
    e.preventDefault();
    Login(details);
  };
  const showPassword = () => {
    setPasswordShown(!passwordShown);
  };
  return (
    <Grid container>
      <Grid item xs={12} md={6} className={classes.leftContainer}>
        <Grid>
          <Typography variant="h3" className={classes.leftMainHeading}>
            Log In to <span className={classes.highlightText}>AutomateGA</span>
          </Typography>
          <form onSubmit={submitHandler} className={classes.loginForm}>
            <Textbox
              varient='form-input'
              id='Username'
              placeholderText='Username'
              name='Username'
              type='text'
              iconVarient='icon'
              onChange={(e) =>
                setDetails({ ...details, email: e.target.value })
              }
              value={details.email}
            />
            <Textbox
              varient='form-input'
              id='Password'
              placeholderText='Password'
              name='Password'
              type={passwordShown ? `text` && <Lock /> : 'password'}
              iconVarient='icon'
              onChange={(e) =>
                setDetails({ ...details, password: e.target.value })
              }
              value={details.password}
            />
            {error !== "" ? (
              <Typography variant="m">
                <span className={classes.invalidCredentials}>{error}</span>
              </Typography>
            ) : (
              ""
            )}
            <div className={classes.pwdSpace}>
              <Typography variant="h3" className={classes.showPassword}>
                Show Password
              </Typography>
              <div className={classes.pwdSwitch}>
                <DSwitch labelVarient="switch selected" inputVarient='input'
                  spanVarient='slider' onClick={showPassword} />
              </div>
              <Button variant='primary' text="Login" data-testid="lgn-btn" />
            </div>
          </form>
          <Typography variant="h6" className={classes.copyright}>
            © 2020 All Rights Reserved.
            <a href="https://www.softcrylic.com/" target="_blank" className={classes.copyrightLink} rel="noreferrer"> Softcrylic LLC. </a>
          </Typography>
        </Grid>
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        style={{ backgroundColor: "#fcfcfd" }}
        sx={{ display: { xs: "none", sm: "none", md: "block" } }}
      >
        <Grid>
          <Typography variant="h3" className={classes.rightMainHeading}>
            <span className={classes.highlightText}>We Make</span> Data Work
          </Typography>
          <Typography variant="h4" className={classes.rightSubHeading}>
            We help organizations turn data chaos into business clarity and
            action.
          </Typography>
          <Typography variant="h6" className={classes.rightParagraph}>
            Our customers work with us to provide strategies, solutions and
            services through the lifecycle of data. We help capture, measure,
            activate, predict trends and deliver results to solve the ongoing
            challenge of running your business in a data-driven manner.
          </Typography>
          <Box
            component="img"
            sx={{
              height: "250px",
              width: "50%",
              margin: "-100px 0 0 200px",
            }}
            alt="omnichannel image."
            src={channel}
          />
        </Grid>
      </Grid>
    </Grid>
  );
}

export default LoginForm;
