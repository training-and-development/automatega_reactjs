import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import LoginForm from "../LoginForm";

const formHeader = () => {
  return render(<LoginForm />);
};

describe("Form is rendered", () => {
  it("should have Username as Placeholder", () => {
    formHeader();
    const emailPlaceholder = screen.getByPlaceholderText(/Username/i);
    expect(emailPlaceholder).toBeInTheDocument();
  });

  it("should have Password as Placeholder", () => {
    formHeader();
    const passwordPlaceholder = screen.getByPlaceholderText(/Password/i);
    expect(passwordPlaceholder).toBeInTheDocument();
  });

  it("should have Username attribute type as email", () => {
    formHeader();
    const emailInput = screen.getByPlaceholderText(/Username/i);
    expect(emailInput).toHaveAttribute("type", "email");
  });

  it("should have Password attribute type as password", () => {
    formHeader();
    const passwordInput = screen.getByPlaceholderText(/Password/i);
    expect(passwordInput).toHaveAttribute("type", "password");
  });

  it("should have Username & Password as required field", () => {
    formHeader();
    const emailInput = screen.getByPlaceholderText(/Username/i);
    expect(emailInput).toHaveAttribute("required");
    const passwordInput = screen.getByPlaceholderText(/Password/i);
    expect(passwordInput).toHaveAttribute("required");
  });

  it("should have Username & Password Icon", () => {
    formHeader();
    const usernameIcon = screen.getByTestId("username-icon");
    expect(usernameIcon).toBeInTheDocument();
    const passwordIcon = screen.getByTestId("password-icon");
    expect(passwordIcon).toBeInTheDocument();
  });

  it("should have Password type as text when show password switch is clicked", () => {
    formHeader();
    screen.getByTestId(`show-password`).click();
    const passwordInput = screen.getByPlaceholderText(/Password/i);
    expect(passwordInput).toHaveAttribute("type", "text");
  });

  //   it("should redirect to homepage after successful login", async () => {
  //     formHeader();
  //     fireEvent.change(screen.getByPlaceholderText(/Username/i), {
  //       target: { value: "admin@softcrylic.com" },
  //     });
  //     fireEvent.change(screen.getByPlaceholderText(/Password/i), {
  //       target: { value: "Kalkaji@123" },
  //     });
  //     fireEvent.click(screen.getByTestId(`lgn-btn`));
  //     //await screen.findByTestId("homepage");
  //     await waitFor(() => {
  //       const hompage = screen.getByTestId("homepage");
  //       expect(homepage).toBeInTheDocument();
  //     });
  //   });
});
