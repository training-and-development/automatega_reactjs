import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  button: {
    fontFamily: "Quicksand !important",
    backgroundColor: "#e2b921",
    fontSize: "14px",
    fontWeight: 600,
    textTransform: "capitalize",
    color: "white",
    float: "right",
  },
  icon: {
    color: "#e2b921",
  },
  fields: {
    fontFamily: "Quicksand !important",
    fontSize: "16px",
    fontWeight: 700,
    color: "#e2b921",
    padding: "11px 0 25px",
  },
  leftContainer: {
    backgroundColor: "#ffffff",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: "0 47px",
  },
  loginForm: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  leftMainHeading: {
    fontFamily: "Quicksand !important",
    fontSize: "40px",
    fontWeight: 600,
    marginBottom: "48px",
    color: "#3b3f5c",
  },
  invalidCredentials: {
    color: "#e7515a",
    fontSize: "14px",
    margin: "4px 0 16px",
  },
  copyright: {
    fontFamily: "Quicksand !important",
    marginTop: "90px",
    fontSize: "14px",
    fontWeight: 600,
    width: "100%",
    color: "#3b3f5c",
  },
  copyrightLink: {
    textDecoration: "none", 
    color: "#e2b921",
  },
  showPassword: {
    fontFamily: "Quicksand !important",
    fontWeight: 600,
    color: "#3b3f5c",
    fontSize: "14px",
    width: "30%",
  },
  rightMainHeading: {
    padding: "48px",
    fontFamily: "Quicksand !important",
    fontSize: "40px",
    fontWeight: 600,
    textAlign: "center",
    color: "#3b3f5c",
  },
  rightSubHeading: {
    color: "#3b3f5c",
    margin: "0 192px 8px",
    padding: "8px",
    fontFamily: "Quicksand !important",
    fontSize: "24px",
    fontWeight: 600,
    textAlign: "center",
  },
  rightParagraph: {
    color: "#3b3f5c",
    margin: "0 96px 8px",
    padding: "8px",
    fontFamily: "Quicksand !important",
    fontSize: "16px",
    fontWeight: "400",
    textAlign: "center",
  },
  switch: {
    color: "secondary",
  },
  highlightText: {
    color: "#e2b921",
  },
  pwdSpace: {
    display: "flex", 
    alignItems: "center",
  },
  pwdSwitch: {
    width: "60%",
  },
}));
