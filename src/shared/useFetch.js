import { useState, useEffect } from 'react'

function useFetch(url) {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    useEffect(() => {
        setLoading(true)
        fetch(url).then(resp => resp.json()).then(resp => setData(resp))
            .catch((err) => setError(err))
            .finally(() => setLoading(false))


    }, [url])
    return { data, loading, error };
}

export default useFetch;