import { useState, useEffect } from 'react';
import axios from 'axios';

const useFetchAxios = (url) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
      setLoading('loading...')
      setData(null);
      setError(null);
      const source = axios.CancelToken.source();
      axios.get(url, { cancelToken: source.token })
      .then(res => {
        setLoading(false);
        res.data && setData(res.data);
      })
      .catch(err => {
          setLoading(false)
          alert('catch')
          setError('An error occurred. Awkward..')
      })
      return () => {
          source.cancel();
      }
  }, [url])
  return { data, loading, error }
}
export default useFetchAxios;