const HOSTNAME = 'http://localhost:4000';

// Json Server urls
export const CLIENTS_URL = `${HOSTNAME}/Clients`;
export const ALL_USERS_URL = `${HOSTNAME}/AllUsers`;
export const FAILED_JOB_LIST_URL = `${HOSTNAME}/FailedJobList`;
export const JOBS_URL = `${HOSTNAME}/Jobs`;

const QUICK_ACCESS_TITLE_1 = "Google Analytics - Data Import";
const QUICK_ACCESS_DESC_1 =
  "Automate and monitor importing offline data into Google Analytics, whether it be campaign taxonomy, product data, or anything.";

const QUICK_ACCESS_TITLE_2 = "Google Analytics - Account Configuration";
const QUICK_ACCESS_DESC_2 =
  "Manage all GA accounts primarily used for Data Import, Property and Audit.";

const QUICK_ACCESS_TITLE_3 = "Property Admin";
const QUICK_ACCESS_DESC_3 = "Automate and scale administering multiple properties to standardize your measurement approach.";

const QUICK_ACCESS_TITLE_4 = "Audit";
const QUICK_ACCESS_DESC_4 = "Automate validating for essential implementation issues, as well as custom dimensions, metrics, content groups, channels, and goals.";

const QUICK_ACCESS_TITLE_5 = "Users";
const QUICK_ACCESS_DESC_5 = "Manage who can access AutomateGA.";

export const QUICK_ACCESS_DATA = [
  {
    title: QUICK_ACCESS_TITLE_1,
    desc: QUICK_ACCESS_DESC_1,
  },
  {
    title: QUICK_ACCESS_TITLE_2,
    desc: QUICK_ACCESS_DESC_2,
  },
  {
    title: QUICK_ACCESS_TITLE_3,
    desc: QUICK_ACCESS_DESC_3,
  },
  {
    title: QUICK_ACCESS_TITLE_4,
    desc: QUICK_ACCESS_DESC_4,
  },
  {
    title: QUICK_ACCESS_TITLE_5,
    desc: QUICK_ACCESS_DESC_5,
  },
];
