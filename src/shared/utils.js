//Create client Data
function createClientData(
  name,
  awsS3Name,
  dataImportAccess,
  propertyAdminAccess,
  auditAccess,
  edit,
) {
  return {
    name,
    awsS3Name,
    dataImportAccess,
    propertyAdminAccess,
    auditAccess,
    edit,
  };
}

export const clientDetailRows = [
  createClientData("Coke", "ccna-ga-automation", true, true, true, 101),
  createClientData(
    "Randstad USA",
    "automate-ga-randstad",
    true,
    false,
    false,
    102
  ),
];

//Create user Data
function createUserData(
  email,
  firstname,
  lastname,
  client,
  isActive,
  isEditable,
  isDeleteable
) {
  return {
    email,
    firstname,
    lastname,
    client,
    isActive,
    isEditable,
    isDeleteable,
  };
}

export const userDetailRows = [
  createUserData(
    "admin@softcrylic.com",
    "Bhupendra",
    "Kumar",
    "COK",
    true,
    true,
    true
  ),
  createUserData(
    "jpbehrens@softcrylic.com",
    "Jean",
    "Paul",
    "COK",
    false,
    true,
    true
  ),
];
