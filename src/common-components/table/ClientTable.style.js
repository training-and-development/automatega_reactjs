import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
    paper: {
        width: "100%"
    },
    table: {
        padding: "20px", 
        marginBottom: "24px", 
        marginTop: "24px"
    },
    tableContainer: {
        marginBottom: "80px"
    },
    tableTitle: {
        fontFamily: "Quicksand",
        fontWeight: "bold",
        padding: "30px 10px",
    },
    tableHeader: {
        "& th": {
            color: "#e2b921",
            borderTop: "1px solid #e0e6ed",
            borderBottom: "1px solid #e0e6ed",
            fontFamily: "Quicksand",
            fontSize: "15px",
            fontWeight: "bold",
        },
    },
    roundedCircle: {
        borderRadius: "50%",
        backgroundColor: "#f1f2f3",
        border: "1px solid transparent",
        height: "40px",
        width: "40px",
    },
    roundedEditCircle: {
        borderRadius: "50%",
        backgroundColor: "#3b3f5c",
        border: "1px solid transparent",
        height: "40px",
        width: "40px",
    },
    activeDot: {
        height: "11px",
        width: "11px",
        borderRadius: "50%",
        cursor: "pointer",
        margin: "0 auto",
        backgroundColor: "#8dbf42",
    },
    inActiveDot: {
        height: "11px",
        width: "11px",
        borderRadius: "50%",
        cursor: "pointer",
        margin: "0 auto",
        backgroundColor: "#e7515a",
    },
}));
