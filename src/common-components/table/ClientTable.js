import React from 'react';
import { Edit2, Trash2, Check, X } from "react-feather";
import PropTypes from "prop-types";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
// import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import useStyles from "./ClientTable.style";

function ClientUserTable(props) {
    const classes = useStyles();
    const { rows, columns } = props;

    const getRow = (column, value) => {
        return (
            <TableCell key={column.id} align={column.align}>
                {column.id === "isEditable" ? (
                    <button className={classes.roundedCircle}>
                        <Edit2 color="#e2b921" id={value} />
                    </button>
                ) : column.id === "isDeleteable" ? (
                    <button className={classes.roundedCircle}>
                        <Trash2 color="#e7515a" id={value} />
                    </button>
                ) : column.id === "active" ? (

                    <div className={value ? classes.activeDot : classes.inActiveDot}>

                    </div>
                ) : column.id === "dataImportAccess" ||
                    column.id === "propertyAdminAccess" ||
                    column.id === "auditAccess" ? (
                    value ? (
                        <Check />
                    ) : (
                        <X />
                    )
                ) : column.id === "edit" ? (
                    <button className={classes.roundedEditCircle}><Edit2 color="#e2b921" id={value} /></button>
                ) : column.format && typeof value === "number" ? (
                    column.format(value)
                ) : (
                    value.toLocaleString()
                )}
            </TableCell>
        );
    };
    return (
        <Paper className={classes.paper}>
            <TableContainer className={classes.tableContainer}>
                <Table stickyHeader aria-label="sticky table" className={classes.table}>
                    <TableHead>
                        <TableRow className={classes.tableHeader}>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows
                            // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                        {columns.map((column) => {
                                            const value = row[column.id];
                                            return getRow(column, value);
                                        })}
                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </TableContainer>
        </Paper>
    );
}
ClientUserTable.defaultProps = {
    rows: "",
    columns: ""
};

ClientUserTable.propTypes = {
    rows: PropTypes.object,
    columns: PropTypes.object
};

export default ClientUserTable;
