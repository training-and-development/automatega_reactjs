import React from "react";
import ClientUserTable from "./ClientTable";
import { userDetailRows } from "../../shared/utils";
import { clientDetailRows } from "../../shared/utils";

export default {
    title: 'Components/ClientUserTable',
    component: ClientUserTable,
}

const Template = args => <ClientUserTable {...args} />;

export const DefaultUserTable = Template.bind({})
DefaultUserTable.args = {
    rows: userDetailRows,
    columns: [
        { id: "email", label: "EMAIL", minWidth: 170 },
        { id: "firstname", label: "FIRST NAME", minWidth: 170 },
        {
            id: "lastname",
            label: "LAST NAME",
            minWidth: 170,
        },
        {
            id: "client",
            label: "CLIENT",
            minWidth: 170,
        },
        {
            id: "isActive",
            label: "ACTIVE",
            minWidth: 70,
            format: (value) => value.toLocaleString("en-US"),
        },
        {
            id: "isEditable",
            label: "",
            minWidth: 100,
            format: (value) => value.toLocaleString("en-US"),
        },
        {
            id: "isDeleteable",
            label: "",
            minWidth: 100,
            format: (value) => value.toLocaleString("en-US"),
        },
    ],
}

export const DefaultClientTable = Template.bind({})
DefaultClientTable.args = {
    rows: clientDetailRows,
    columns: [
        { id: "name", label: "NAME", minWidth: 170 },
        { id: "awsS3Name", label: "AWS S3 BUCKET NAME", minWidth: 100 },
        {
            id: "dataImportAccess",
            label: "DATA IMPORT ACCESS",
            minWidth: 170,
        },
        {
            id: "propertyAdminAccess",
            label: "PROPERTY ADMIN ACCESS",
            minWidth: 170,
        },
        {
            id: "auditAccess",
            label: "AUDIT ACCESS",
            minWidth: 170,
        },
        {
            id: "edit",
            label: "EDIT",
            minWidth: 170,
            format: (value) => value.toLocaleString("en-US"),
        },
    ],
}