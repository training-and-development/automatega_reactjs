import React from "react";
import DataImportCard from "./DataImportCard";
import { Send, ThumbsUp, ThumbsDown } from "react-feather";

export default {
    title: 'Components/DataImportCard',
    component: DataImportCard,
}

const Template = args => <DataImportCard {...args} />;

export const TotalCard = Template.bind({})
TotalCard.args = {
    icon: <Send color="#ffffff" size={50} strokeWidth={1} />,
    title: "Total Data Import Job Ran",
    value: 562,
    index: 0,
}

export const SuccessCard = Template.bind({})
SuccessCard.args = {
    icon: <ThumbsUp color="#ffffff" size={50} strokeWidth={1} />,
    title: "Successful Data Import",
    value: 431,
    index: 1,
}

export const FailedCard = Template.bind({})
FailedCard.args = {
    icon: <ThumbsDown color="#ffffff" size={50} strokeWidth={1} />,
    title: "Failed Data Import",
    value: 130,
    index: 2,
}