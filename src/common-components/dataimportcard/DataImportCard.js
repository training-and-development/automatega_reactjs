import React from 'react';
import { experimentalStyled as styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import useStyles from "./DataImportCard.style";
import { Send } from "react-feather";

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
    height: "100%",
}));

const DataImportCard = (props) => {
    const classes = useStyles();
    const { icon, title, index, value } = props;
    const { firstStyle, secondStyle, thirdStyle, sendCount, upCount, downCount, sendIcon, upIcon, downIcon } = useStyles();
    const styles = [firstStyle, secondStyle, thirdStyle];
    const countStyles = [sendCount, upCount, downCount];
    const iconStyles = [sendIcon, upIcon, downIcon];
    return (
        <Grid item xs={2} sm={4} md={4} key={index}>
            <Item className={`${styles[index]}`}>
                <div className={`${iconStyles[index]}`}>
                    {icon}
                </div>
                <Typography
                    gutterBottom
                    variant="subtitle1"
                    className={classes.jobTitle}
                >
                    {title}
                </Typography>
                <Typography
                    gutterBottom
                    variant="h5"
                    className={`${countStyles[index]}`}
                >
                    {value}
                </Typography>
            </Item>
        </Grid>
    );
};

DataImportCard.defaultProps = {
    index: 0,
    title: "Google Analytics - Data Import",
    value: 562,
    icon: <Send color="#ffffff" size={50} strokeWidth={1} />,
};

DataImportCard.propTypes = {
    index: PropTypes.oneOf([0, 1, 2]),
    title: PropTypes.string,
    value: PropTypes.number,
    icon: PropTypes.node,
};

export default DataImportCard;