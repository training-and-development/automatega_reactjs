import React from 'react';
import DSwitch from './Switch';

export default {
    title: 'Components/Switch',
    component: DSwitch,
}

const Template = args => <DSwitch {...args} />;

export const Switch = Template.bind({})
Switch.args = {
    labelVarient: 'switch selected',
    inputVarient: 'input',
    spanVarient: 'slider'
}