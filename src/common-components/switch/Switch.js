import React from 'react';
import PropTypes from 'prop-types';
import './Switch.css';

function DSwitch(props) {
    const { labelVarient, inputVarient, spanVarient, onClick } = props;
    return (
        <label className={`${labelVarient}`}>
            <input type='checkbox' className={`${inputVarient}`} onClick={onClick} />
            <span className={`${spanVarient}`}></span>
        </label>
    )
}

DSwitch.propTypes = {
    labelVarient: PropTypes.string,
    inputVarient: PropTypes.string,
    spanVarient: PropTypes.string,
    onClick: PropTypes.func
}

DSwitch.defaultProps = {
    labelVarient: "switch selected",
    inputVarient: "input",
    spanVarient: "slider",
}

export default DSwitch;