import React from 'react';
import PropTypes from 'prop-types';
import { User } from "react-feather";
import { Lock } from "react-feather";
import { makeStyles } from '@material-ui/core';
import './textbox.css';

const useStyles = makeStyles({
    txtBoxSpacing: {
        display: 'flex', 
        padding: '11px 0 25px 0'
    },
});

function Textbox(props) {
    const { varient, id, placeholderText, name, type, iconVarient, onChange, value } = props;
    const classes = useStyles();
    return (
        <div className={classes.txtBoxSpacing}>
            <div className={`${iconVarient}`}>
                {type === 'text' ? <User /> : <Lock />}
            </div>
            <input
                id={id}
                className={`${varient}`}
                placeholder={placeholderText}
                name={name}
                type={type}
                onChange={onChange}
                value={value}
            />
        </div>
    )
}
Textbox.propTypes = {
    placeholderText: PropTypes.string,
    type: PropTypes.oneOf(["text", "password"]),
    varient: PropTypes.oneOf(["form-input"]),
    iconVarient: PropTypes.oneOf(["icon"]),
    name: PropTypes.string,
    id: PropTypes.string
}

Textbox.defaultProps = {
    varient: "form-input",
    placeholderText: "Username",
    name: "Username",
    id: "Username",
    type: "text ",
    iconVarient: "icon"
}

export default Textbox;