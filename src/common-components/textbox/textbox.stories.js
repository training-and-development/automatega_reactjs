import React from "react";
import Textbox from "./textbox";

export default {
    title: 'Components/textbox',
    component: Textbox,
}

const Template = args => <Textbox {...args} />;

export const Inputfield = Template.bind({})
Inputfield.args = {
    varient: 'form-input',
    placeholderText: 'Username',
    type: 'text',
    name: "Username",
    id: "Username",
    iconVarient: "icon"
}

// export const PasswordInputfield = Template.bind({})
// PasswordInputfield.args = {
//     varient: 'form-input',
//     placeholderText: 'Password',
//     type: 'password',
//     name: 'Password',
//     iconVarient: "icon"
// }