import React from 'react'
import { experimentalStyled as styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import useStyles from "./QuickAccess.style";
import { Box as BoxIcon } from "react-feather";

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    padding: theme.spacing(2),
    textAlign: "left",
    color: theme.palette.text.secondary,
    height: "100%",
}));

const QuickAccessCards = (props) => {
    const classes = useStyles();
    const { icon, index, title, desc } = props;
    const { firstStyle, secondStyle, thirdStyle, fourthStyle, fifthStyle } =
        useStyles();
    const styles = [firstStyle, secondStyle, thirdStyle, fourthStyle, fifthStyle];
    return (
        <Grid item xs={2} sm={4} md={4} key={index}>
            <Item className={`${styles[index]}`}>
                {icon}
                <Typography
                    gutterBottom
                    variant="subtitle1"
                    className={classes.quickAccessTitle}
                >
                    {title}
                </Typography>
                <Typography
                    gutterBottom
                    variant="p"
                    className={classes.quickAccessDesc}
                >
                    {desc}
                </Typography>
                <Grid item>
                    <Typography
                        gutterBottom
                        sx={{ cursor: "pointer" }}
                        variant="body2"
                        className={classes.quickAccessDiscoverLink}
                    >
                        Discover -{">"}
                    </Typography>
                </Grid>
            </Item>
        </Grid>
    );
};
QuickAccessCards.defaultProps = {
    index: 0,
    title: "Google Analytics - Data Import",
    desc: "Automate and monitor importing offline data into Google Analytics, whether it be campaign taxonomy, product data, or anything.",
    icon: <BoxIcon color="#c54922" size={50} strokeWidth={1} />,
};

QuickAccessCards.propTypes = {
    index: PropTypes.oneOf([0, 1, 2, 3, 4]),
    title: PropTypes.string,
    desc: PropTypes.string,
    icon: PropTypes.node,
};

export default QuickAccessCards