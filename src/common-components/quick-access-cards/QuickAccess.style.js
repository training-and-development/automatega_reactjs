import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  quickAccessTitle: {
    fontFamily: "Quicksand !important",
    fontWeight: "bold !important",
    letterSpacing: "2px !important",
    fontSize: "20px !important",
  },
  quickAccessDesc: {
    fontFamily: "Quicksand !important",
  },
  quickAccessDiscoverLink: {
    fontFamily: "Quicksand !important",
    color: "#e21b76 !important",
  },
  firstStyle: {
    backgroundColor: "#f5b2ce40 !important",
  },
  secondStyle: {
    backgroundColor: "#c549222b !important",
  },
  thirdStyle: {
    backgroundColor: "#D6E0B8 !important",
  },
  fourthStyle: {
    backgroundColor: "#F3E8C3 !important",
  },
  fifthStyle: {
    backgroundColor: "#8256c53b !important",
  },
}));
