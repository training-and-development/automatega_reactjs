import React from "react";
import QuickAccessCards from "./QuickAccessCards";
import { Box as BoxIcon } from "react-feather";

export default {
    title: 'Components/QuickAccessCard',
    component: QuickAccessCards,
}

const Template = args => <QuickAccessCards {...args} />;

export const QuickAccessCard = Template.bind({})
QuickAccessCard.args = {
    icon: <BoxIcon color="#c54922" size={50} strokeWidth={1} />,
    title: "Google Analytics - Data Import",
    desc: "Automate and monitor importing offline data into Google Analytics, whether it be campaign taxonomy, product data, or anything.",
    index: 0,
}