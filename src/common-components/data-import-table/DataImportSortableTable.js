import React from 'react';
import PropTypes from "prop-types";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import useStyles from "./DataImportSortableTable.style";
import { InputAdornment, TableSortLabel, TextField } from '@mui/material';
import { Search } from 'react-feather';

function DataImportSortableTable(props) {
    const classes = useStyles();
    const { rows, columns, searchBy, tableTitle } = props;
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [order, setOrder] = React.useState("asc");
    const [orderBy, setOrderBy] = React.useState();
    const [searchedVal, setSearchedVal] = React.useState("");

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const getRow = (column, value) => {
        return (
            <TableCell key={column.id} align={column.align}>

                {column.id === "jobStatus" ? (
                    <div className={value !== "FAILED" ? classes.activeDot : classes.inActiveDot}>
                    </div>
                ) : column.id === "extractionSetupData" ? (
                    <>
                        <div>{value}</div>
                        {/* <div><b>Id : </b>{value.webPropertyId}</div>
                        <div><b>Name : </b>{value.webPropertyName}</div> */}
                    </>
                ) : column.id === "excpMsg" ? (
                    <>
                        {/* {value} */}
                        {JSON.parse(value.replace("403 Forbidden", ""))["message"]}
                    </>
                ) : column.format && typeof value === "number" ? (
                    column.format(value)
                ) : (
                    value
                )}
            </TableCell>
        );
    };

    const descendingComparator = (a, b, orderBy) => {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }

    const getComparator = (order, orderBy) => {
        return order === "desc"
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }

    const stableSort = (array, comparator) => {
        const stabilizedThis = array?.map((el, index) => [el, index]);
        stabilizedThis?.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis?.map(el => el[0]);
    }
    const createSortHandler = property => event => {
        handleRequestSort(event, property);
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(property);
    };

    return (
        <Paper className={classes.paper}>
            <div className={classes.tableSpacing}>
                <div className={classes.tableDesc}>{tableTitle}</div>
                <TableContainer className={classes.tableContainer}>
                    <TextField
                        onChange={(e) => setSearchedVal(e.target.value)}
                        placeholder='Search...'
                        className={classes.searchBox}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <Search className={classes.searchIcon} />
                                </InputAdornment>
                            ),
                        }}
                    />
                    <Table stickyHeader aria-label="sticky table" className={classes.table}>
                        <TableHead>
                            <TableRow className={classes.tableHeader}>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        align={column.align}
                                        style={{ minWidth: column.minWidth }}
                                        sortDirection={orderBy === column.id ? order : false}
                                    >
                                        <TableSortLabel
                                            active={orderBy === column.id}
                                            direction={orderBy === column.id ? order : "asc"}
                                            onClick={createSortHandler(column.id)}
                                        >
                                            {column.label}
                                        </TableSortLabel>
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {stableSort(rows, getComparator(order, orderBy))
                                ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .filter((row) =>
                                    !searchedVal?.length || row[searchBy]
                                        .toString()
                                        .toLowerCase()
                                        .includes(searchedVal.toString().toLowerCase())
                                )
                                .map((row) => {
                                    return (
                                        <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                            {columns.map((column) => {
                                                const value = row[column.id];
                                                return getRow(column, value);
                                            })}
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
            <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows?.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </Paper>
    );
}
DataImportSortableTable.defaultProps = {
    rows: "",
    columns: ""
};

DataImportSortableTable.propTypes = {
    rows: PropTypes.object,
    columns: PropTypes.object
};

export default DataImportSortableTable;
