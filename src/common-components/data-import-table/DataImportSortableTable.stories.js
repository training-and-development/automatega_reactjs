import React from "react";
import DataImportSortableTable from "./DataImportSortableTable";
import { dataImportFailedRows } from "../../component-features/dataImport/utils";
import { dataImportJobRows } from "../../component-features/dataImport/utils"

export default {
    title: 'Components/DataImportSortableTable',
    component: DataImportSortableTable,
}

const Template = args => <DataImportSortableTable {...args} />;

export const FailedImportTable = Template.bind({})
FailedImportTable.args = {
    rows: dataImportFailedRows,
    columns: [
        { id: "requestFrom", label: "REQUEST FROM", minWidth: 150 },
        { id: "fileName", label: "FILE NAME", minWidth: 150 },
        {
            id: "customerDataSourceId",
            label: "CUSTOMER DATA SOURCE ID",
            minWidth: 170,
        },
        {
            id: "message",
            label: " MESSAGE",
            minWidth: 50,
        },
        {
            id: "jobStatus",
            label: "JOB STATUS",
            minWidth: 10,
            format: (value) => value.toLocaleString("en-US"),
        },
        {
            id: "fileReceived",
            label: "FILE RECEIVED",
            minWidth: 170,
            format: (value) => value.toLocaleString("en-US"),
        }
    ],
    tableTitle: 'Data Import Job Failed due to Insufficient Permission issue'
}

export const SuccessImportTable = Template.bind({})
SuccessImportTable.args = {
    rows: dataImportJobRows,
    columns: [
        { id: "requestFrom", label: "REQUEST FROM", minWidth: 150 },
        { id: "fileName", label: "FILE NAME", minWidth: 150 },
        {
            id: "customerDataSourceId",
            label: "CUSTOMER DATA SOURCE ID",
            minWidth: 170,
        },
        {
            id: "jobStatus",
            label: "JOB STATUS",
            minWidth: 10,
            format: (value) => value.toLocaleString("en-US"),
        },
        {
            id: "fileReceived",
            label: "FILE RECEIVED",
            minWidth: 170,
            format: (value) => value.toLocaleString("en-US"),
        }
    ],
    tableTitle: 'Data Import Job Details'
}