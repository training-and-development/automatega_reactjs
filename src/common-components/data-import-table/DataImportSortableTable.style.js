import { makeStyles } from "@material-ui/core/styles";
export default makeStyles((theme) => ({
    paper: {
        width: "100%",
    },
    tableTitle: {
        fontFamily: "Quicksand",
        fontWeight: "bold",
        padding: "30px 10px",
    },
    tableHeader: {
        "& th": {
            color: "#e2b921",
            borderTop: "1px solid #e0e6ed",
            borderBottom: "1px solid #e0e6ed",
            fontFamily: "Quicksand",
            fontSize: "15px",
            fontWeight: "bold",
        },
    },
    activeDot: {
        height: "11px",
        width: "11px",
        borderRadius: "50%",
        cursor: "pointer",
        margin: "0 auto",
        backgroundColor: "#8dbf42",
    },
    inActiveDot: {
        height: "11px",
        width: "11px",
        borderRadius: "50%",
        cursor: "pointer",
        margin: "0 auto",
        backgroundColor: "#e7515a",
    },
    tableSpacing: {
        padding: "20px",
    },
    tableContainer: {
        padding: "20px",
        margin: "24px 0px"
    },
    tableDesc: {
        fontWeight: "600",
        fontSize: "17px",
        padding: "16px 15px",
        fontFamily: "Quicksand"
    },
    table: {
        paddingTop: "50px"
    },
    searchBox: {
        float: 'right',
        width: "350px",
        padding: "8px 30px 8px 14px",
        boxShadow: "2px 5px 17px 0 rgb(31 45 61 / 10%)",
        border: "1px solid #e0e6ed",
        borderRadius: "6px",
        marginTop: "8px"
    },
    searchIcon: {
        width: "20px",
        height: "20px",
        color: '#d3d3d3'
    },
}));
