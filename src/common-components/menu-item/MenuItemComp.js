import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, MenuItem } from '@material-ui/core';
import './MenuItemComp.css';

const useStyles = makeStyles({
    root: {
        fontSize: '13px',
        fontWeight: 600,
        fontFamily: 'Quicksand',
        color: '#3B3F5C',
        boxSizing: 'content-box',
        padding: '9px 16px',
        "&:hover": {
            backgroundColor: "#FCF8E8",
        }
    },
    element: {
        width: '100%',
        "&:hover": {
            color: "#e2b921",
        }
    }
});

function MenuItemComp(props) {
    const { onClick, icon, iconText, divider, style } = props;
    const classes = useStyles();
    return (
        <>
            <MenuItem divider={divider} style={style} onClick={onClick} className={classes.root} disableRipple>
                {icon}
                {divider === false ? <span className={classes.element}>{iconText}</span> : <span>{iconText}</span>}
            </MenuItem>
        </>
    )
}

MenuItemComp.propTypes = {
    onClick: PropTypes.func,
    iconText: PropTypes.string,
    icon: PropTypes.node,
    divider: PropTypes.bool
}
MenuItemComp.defaultProps = {
    onClick: "",
    iconText: "On Board Client",
    icon: "",
    divider: true
}

export default MenuItemComp