import React from 'react';
import MenuItemComp from './MenuItemComp';
import { User } from "react-feather";

export default {
    title: 'Components/MenuItem',
    component: MenuItemComp,
}

const Template = args => <MenuItemComp {...args} />

export const PrimaryMenuItem = Template.bind({})
PrimaryMenuItem.args = {
    icon: <User size={18} style={{ marginRight: '7px' }} />,
    iconText: "On Board Client",
    varient: "menus",
    divider: true,
}

export const SecondaryMenuItem = Template.bind({})
SecondaryMenuItem.args = {
    icon: "",
    iconText: "Coke",
    varient: "menus",
    divider: false,
}