import React from 'react'
import PropTypes from 'prop-types';
import { makeStyles, IconButton } from '@material-ui/core';
import Grid from "@mui/material/Grid";
import './IconButtonComp.css';

const useStyles = makeStyles({
    root: {
        borderRadius: '0%',
        padding: '5px',
        "&:hover": {
            backgroundColor: "transparent",

        }
    },
    btn: {
        backgroundColor: 'transparent', 
        border: '1px solid transparent', 
        cursor: 'pointer'
    },
    icon1: {
        color: '#888EA8', 
        padding: '0 8px 0 0', 
        marginTop: '5px'
    },
    icon2: {
        color: '#888EA8', 
        marginTop: '5px'
    }
});

function IconButtonComp(props) {
    const { onClick, varient, text, icon1, icon2 } = props
    const classes = useStyles();
    return (
        <IconButton
            onClick={onClick}
            disableRipple={true}
            size="small"
            sx={{ ml: 2 }}
            className={classes.root}
            // aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            //aria-expanded={open ? "true" : undefined}
            data-testid="account-menu-btn"

        >
            <Grid margin="0 8px 0 8px">
                <button className={classes.btn}>
                    <span className={classes.icon1}>{icon1}</span>
                    <span className={varient}>{text}</span>
                    <span className={classes.icon2}>{icon2}</span>
                </button>
            </Grid>
        </IconButton>
    )
}

IconButtonComp.propTypes = {
    onClick: PropTypes.func,
    varient: PropTypes.oneOf(["primary-title", "secondary-title"]),
    text: PropTypes.string,
    icon1: PropTypes.node,
    icon2: PropTypes.node,
}

IconButtonComp.defaultProps = {
    onClick: "",
    varient: "primary-title",
    text: "Bhupendra",
    icon1: "",
    icon2: "",
}


export default IconButtonComp