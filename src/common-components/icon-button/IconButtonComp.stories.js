import React from "react";
import IconButtonComp from "./IconButtonComp";
import { User, ChevronDown } from "react-feather";

export default {
    title: 'Components/IconButton',
    component: IconButtonComp,
}

const Template = args => <IconButtonComp {...args} />;

export const IconButton = Template.bind({})
IconButton.args = {
    varient: 'primary-title',
    icon1: <User size={15} />,
    icon2: <ChevronDown size={15} />,
    text: 'Bhupendra',

}

export const withoutIconButton = Template.bind({})
withoutIconButton.args = {
    varient: 'secondary-title',
    text: 'Randstad USA',
    icon1: "",
    icon2: "",

}