import React from 'react'
import Menu from "@mui/material/Menu";
import './MenuComp.css';

function MenuComp(props) {
    const { onClose, onClick, varient, open, id, PaperProps,
        transformOrigin, anchorOrigin, anchorEl, menuItem1, menuItem2, menuItem3 } = props
    return (
        <>
            <Menu
                anchorEl={anchorEl}
                id={id}
                open={open}
                onClose={onClose}
                onClick={onClick}
                PaperProps={PaperProps}
                transformOrigin={transformOrigin}
                anchorOrigin={anchorOrigin}
                className={varient}
            >
                {menuItem1}
                {menuItem2}
                {menuItem3}
            </Menu>
        </>
    )
}

export default MenuComp