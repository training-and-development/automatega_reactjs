import React from "react";
import MenuComp from "./MenuComp";

export default {
    title: 'Components/Menu',
    component: MenuComp,
}

const Template = args => <MenuComp {...args} />;

export const menu = Template.bind({})
menu.args = {
    varient: "menus",
}