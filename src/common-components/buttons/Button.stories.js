import React from 'react';
import Button from './Button';
import { Plus } from "react-feather";

export default {
    title: 'Components/Button',
    component: Button,
    argTypes: { onClickHandler: { action: "onClickHandler" } }
}
const Template = args => <Button {...args} />;

export const defaultButton = Template.bind({})
defaultButton.args = {
    varient: "primary",
    text: "Log In",
    size: "md",
}

export const iconButton = Template.bind({})
iconButton.args = {
    varient: "primary",
    text: "Add New User",
    size: "md",
    icon: <Plus style={{ "paddingRight": "5px" }} />
}