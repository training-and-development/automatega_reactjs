import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';
function Button(props) {
    const { varient, text, size, icon, onClickHandler } = props;
    let scale = 1
    if (size === "sm") scale = 0.75
    if (size === 'lg') scale = 1.75
    const style = {
        padding: `${scale * 0.5}rem ${scale * 1}rem`,
        border: 'none',
    }
    return (
        <button className={`button ${varient}`} onClick={onClickHandler} style={style}>{icon}{text}</button>
    )
}

Button.propTypes = {
    text: PropTypes.string,
    size: PropTypes.oneOf(["md", "sm", "lg"]),
    varient: PropTypes.oneOf(["primary", "secondary"]),
    onClickHandler: PropTypes.func,
}

Button.defaultProps = {
    varient: "primary",
    text: "Hello World",
    size: "md"
}


export default Button;