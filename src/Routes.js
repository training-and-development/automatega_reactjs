import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Login from "./pages/Login";
import Homepage from "./pages/Homepage";
import UserDetailsPage from "./pages/UserDetailsPage";
import DataImportDashboard from "./pages/DataImportDashboard";
import PropertyAdminDashboard from "./pages/PropertyAdminDashboard";
import AuditDashboard from "./pages/AuditDashboard";
import GaAccounts from "./pages/GaAccounts";
import AuthApi from "./api/AuthApi";

const PageRoutes = () => {
  const Auth = useContext(AuthApi);
  return (
    <Router>
      <Switch>
        <ProtectedLogin exact auth={Auth.auth} path="/" component={Login} />
        <ProtectedRoute auth={Auth.auth} path="/home" component={Homepage} />
        <ProtectedRoute
          auth={Auth.auth}
          path="/users"
          component={UserDetailsPage}
        />
        <ProtectedRoute
          auth={Auth.auth}
          path="/dataimportdashboard"
          component={DataImportDashboard}
        />
        <ProtectedRoute
          auth={Auth.auth}
          path="/propertyadmin"
          component={PropertyAdminDashboard}
        />
        <ProtectedRoute
          auth={Auth.auth}
          path="/auditdashboard"
          component={AuditDashboard}
        />
        <ProtectedRoute
          auth={Auth.auth}
          path="/gaaccountsd"
          component={GaAccounts}
        />
      </Switch>
    </Router>
  );
};

const ProtectedRoute = ({ auth, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => (auth ? <Component /> : <Redirect to="/" />)}
    />
  );
};

const ProtectedLogin = ({ auth, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => (!auth ? <Component /> : <Redirect to="/home" />)}
    />
  );
};

export default PageRoutes;
