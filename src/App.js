import React, { useState, useEffect } from "react";
import PageRoutes from "./Routes";
import "@fontsource/quicksand";
import AuthApi from "./api/AuthApi";
import Cookies from "js-cookie";

function App() {
  const [auth, setAuth] = useState(false);
  const readCookie = () => {
    const user = Cookies.get("user");
    if (user) {
      setAuth(true);
    }
  };
  useEffect(() => {
    readCookie();
  }, []);

  return (
    <AuthApi.Provider value={{ auth, setAuth }}>
      <PageRoutes />
    </AuthApi.Provider>
  );
}

export default App;
